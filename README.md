# README #

This repository contains my own code experiments in the reading of buildyourownlisp.

Based on the book from <http://buildyourownlisp.com/chapter2_installation>

## Chapter 2 ##

Files:
* hello-world.c The C source for hello-world
* build.sh The script to build hello-world

Once compiled, run `hello-world.exe`

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
